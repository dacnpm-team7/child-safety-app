const DefaultTexts = {
    // Validations.
    INVALID_USERNAME: "Email không hợp lệ.",
    USERNAME_ALREADY_EXISTS: "Tài khoản đã tồn tại.",
    INVALID_PASSWORD: "Mật khẩu cần có ít nhất 8 ký tự, ít nhất 1 chữ cái & 1 số.",
    INVALID_CONFIRM_PASSWORD: "Nhập lại mật khẩu không đúng.",
    INVALID_PERSON_NAME: "Xin nhập đầy đủ họ và tên.",

    // UI.
    REMEMBER_ME: "Lưu tài khoản.",
    SAVE_ACCOUNT_ERROR: "Cập nhật thông tin không thành công.",
    SAVE_ACCOUNT_SUCCESS: "Cập nhật thông tin thành công.",
    LOAD_ERROR_DETAUL_MESSAGE: "Có lỗi xảy ra khi load dữ liệu, xin thử lại!",

    // Operations:
    ACCOUNT_CREATED: "Tạo tài khoản thành công.",
    AUTH_ERROR: "Email hoặc mật khẩu không đúng.",
}

export const Texts = DefaultTexts;