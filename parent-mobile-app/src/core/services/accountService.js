import BaseService from './baseService'
import { accountsEndpoint } from '../apis/endpoints'
import { Texts } from '../texts';

class AccountService extends BaseService {
    constructor(){
        super(accountsEndpoint)
    }

    async login(username, password) {
        const result = await this.query({ username, password });
        if (result !== null && result.success)
            return { account: result.account };

        return { error: Texts.AUTH_ERROR };
    }

    async signup(username, password) {
        return await this.createNewAccount(username, password);
    }

    async createNewAccount(username, password) {
        const checkUsername = await this.isValidUsername(username);
        if (checkUsername.error)
            return checkUsername;

        if (!validatePassword(password))
            return { error: Texts.INVALID_PASSWORD };

        return await this.create({ username, password });
    }

    /**
     * @param {string} username 
     */
    async isValidUsername(username) {
        if (!validateUsername(username))
            return { error: Texts.INVALID_USERNAME };

        const existAccount = await this.query({ username: username });
        if (existAccount.data.length > 0)
            return { error: Texts.USERNAME_ALREADY_EXISTS };

        return { username: username }
    }
}

export default new AccountService();