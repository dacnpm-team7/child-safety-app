import { Texts } from '../../texts';
import { authSignInEndpoint } from '../../apis/endpoints';
import BaseService from '../baseService';

class SignInService extends BaseService {
    constructor(){
        super(authSignInEndpoint)
    }
 
    async login(username, password) {
        //const result = await this.query({ username, password });
        // if (result !== null && result.success)
        //     return { account: result.account };

        // return { error: Texts.AUTH_ERROR };
        return await this.create({ username, password });
    }
}

export default new SignInService();


