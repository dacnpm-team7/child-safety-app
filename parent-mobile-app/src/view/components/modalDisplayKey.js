import React, { Component } from 'react'
import { Text, View, Modal } from 'react-native'
import { MaterialIcons } from '@expo/vector-icons'

export default function ModalDisplayKey(props){
   const [modalOpen, setModalOpen] = useState(false)

    return (
        <View>
            <Modal visible={modalOpen} animationType='slide'>
            <View style={styles.modalContent}>
                <MaterialIcons
                name='close'
                size={24}
                onPress={() => setModalOpen(false)}
                style={{...styles.modalToggle, ...styles.modalClose}}
                />

                <Text> Nhập khóa này vào thiết bị của con bạn: {this.props.parentKey} </Text>

            </View>
            </Modal>
        </View>
    )
}

const styles = StyleSheet.create({
    modalToggle: {
        marginBottom: 10,
        borderWidth: 1,
        borderColor: 'red',
        padding: 10,
        borderRadius: 10,
        alignSelf: 'center'
    },
    modalClose: {
        marginTop: 20,
        marginBottom: 0
    },
    modalContent: {
        flex: 1,
    }
})