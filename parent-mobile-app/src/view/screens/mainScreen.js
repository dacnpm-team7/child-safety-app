import React from 'react';
import { StyleSheet} from 'react-native';
import { BottomNavigation, BottomNavigationTab, Icon } from '@ui-kitten/components';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import MapScreen from '../../../screens/MapScreen';
import HistoryTrackingScreen from './parent/historyTrackingScreen';
import RecordListeningScreen from './parent/recordListeningScreen';
import SettingScreen from './parent/settingScreen';
import { SafeAreaView } from 'react-navigation'; 
import WelcomeScreen from '../../../screens/WelcomeScreen';

const Tab = createBottomTabNavigator();

const TabBarComponent = ({ navigation, state }) => {

  const onSelect = (index) => {
      const screenName = state.routeNames[index];
      navigation.navigate(screenName);
  };

  const tabIcon = (style, name) => (
      <Icon {...style} name={name} />
  );

  return (
      <SafeAreaView>
          <BottomNavigation
              appearance="noIndicator"
              selectedIndex={state.index}
              onSelect={onSelect}
              style={{ borderTopColor: "#e6e6e6", borderTopWidth: 0.5 }}
          >
              <BottomNavigationTab icon={(style) => tabIcon(style, "map-outline")} title="Bản đồ" />
              <BottomNavigationTab icon={(style) => tabIcon(style, "book-outline")} title="Lịch sử" />
              <BottomNavigationTab icon={(style) => tabIcon(style, "recording-outline")} title="Liên lạc" />
              <BottomNavigationTab icon={(style) => tabIcon(style, "settings-2-outline")} title="Cài đặt" />
          </BottomNavigation>
      </SafeAreaView>
  );
};

export default function MainScreen(props) {
  
  
  return (
    <SafeAreaView style={{ flex: 1 }}>
        <Tab.Navigator initialRouteName="Map" tabBar={props => <TabBarComponent {...props} />}>
            <Tab.Screen name="Map" component={MapScreen} />
            <Tab.Screen name="HistoryTracking" component={HistoryTrackingScreen} />
            <Tab.Screen name="RecordListening"  component={RecordListeningScreen} />
            <Tab.Screen name="Setting" component={SettingScreen} />
        </Tab.Navigator>
    </SafeAreaView>
);
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center'
  }
});