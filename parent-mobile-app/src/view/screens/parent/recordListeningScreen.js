import React from 'react'
import {Image,TouchableHighlight, TouchableOpacity,  View, Text,StyleSheet } from 'react-native'
import { globalStyles,images } from '../../../global/styles';


export default function RecordListeningScreen({ navigation }){
    return (
        <View style={styles.container}>
            <Text style ={ styles.title}>Findout what happen around your kid phone</Text>

           <View style = {styles.rowIcons}>

           <View style = {styles.buttonContainer}>
           <TouchableOpacity onPress= {onRecordButtonPress}>
           <Image style = {styles.buttonImage}  source = {images.microIcon}/>
           </TouchableOpacity>
            <Text style = {styles.iconDescription}>Record the sound near your kid</Text>
            </View>

            <View style = {styles.buttonContainer}>
            <TouchableOpacity onPress= {onListeningButtonPress}>
            <Image style = {styles.buttonImage} source = {images.listenerIcon}/>
            </TouchableOpacity>
            <Text style = {styles.iconDescription}>Start the listening </Text>
            </View>

            </View>
        </View>
    )
}

const onRecordButtonPress = ()=>{
	alert('Record button press goes here')
}
const onListeningButtonPress = () =>{
	alert('Listening button press goes here')
}



const styles = StyleSheet.create({
	container:{
		flex: 1,
		marginTop:100,
		alignItems:'center'
	},

	title:{
		width:350,
		fontSize:24,
		textAlign:'center',
		marginBottom:50
	},
	rowIcons:{
		justifyContent:'space-around',
		flexDirection:'row',
	},

	iconDescription:{
		marginTop:20,
		fontSize:15,
		textAlign:'center',
		width:150
	},

	buttonContainer:{
		alignItems:'center'
	},
	
	buttonImage:{
		width:130,
		height:130
	}
});


