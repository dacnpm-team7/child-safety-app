import React from 'react'
import { View, Text } from 'react-native'
import { globalStyles } from '../../../constants/globalConstant'

export default function ChildSettingScreen({ navigation }){
    return (
        <View style={globalStyles.container}>
            <Text>ChildSettingScreen</Text>
        </View>
    )
}

