import { globalActionTypes } from "./actionTypes";

export const addChildren = (uuid, name, lastestLocation) => ({
    type: globalActionTypes.MAPDATA_ADD_CHILDREN,
    payload: { uuid, name, lastestLocation }
});

export const addParentKey = (parentKey) => ({
    type: globalActionTypes.MAPDATA_ADD_PARENTKEY,
    payload: { parentKey }
});

export const addNickNameChildren = (uuid, nickname) => ({
    type: globalActionTypes.MAPDATA_ADD_NICKNAME_CHILDREN,
    payload: { uuid, nickname }
});

export const addLatestChildrenLocation = (uuid, location) => ({
    type: globalActionTypes.MAPDATA_ADD_LASTEST_CHILDREN_LOCATION,
    payload: { uuid, location }
});

export const deleteAllData = () => ({
    type: globalActionTypes.MAPDATA_DELETE_ALL_DATA,
});