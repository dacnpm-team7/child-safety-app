import { combineReducers } from "redux";
import userReducer from "./userReducer";

const AppReducer = combineReducers({
    userReducer
});

export default AppReducer;