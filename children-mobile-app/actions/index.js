const Login = (usertoken) => ({
    type: 'LOG_IN',
    userToken: usertoken
});

const Logout = () => ({
    type: 'LOG_OUT'
});

const SetUserInfo = (userInfo) => ({
    type: 'GET_USER_INFO',
    userInfo: userInfo
});

const RestoreUserToken = (usertoken) => ({
    type: 'RESTORE_TOKEN',
    userToken: usertoken
});
export { Login, Logout, SetUserInfo, RestoreUserToken };