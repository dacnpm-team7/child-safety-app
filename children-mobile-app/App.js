import React, { Component } from 'react';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import SplashScreen from './screens/SplashScreen';
import { createDrawerNavigator } from '@react-navigation/drawer';
import { Image, Button, View, Text } from "react-native";
import { AppLoading } from "expo";
import * as Font from "expo-font";
import { theme } from './constants';

import WelcomeScreen from './screens/WelcomeScreen';
import ConnectedScreen from './screens/ConnectedScreen';
// import RegisterScreen from './screens/RegisterScreen';
// import LoginScreen from './screens/LoginScreen';
import ChatScreen from './screens/ChatScreen';
import ContactScreen from './screens/ContactScreen';
import AddParentsScreen from './screens/AddParents';
//import DrawerMenu from "./screens/DrawerMenu";

import { createStore, applyMiddleware } from 'redux';
import { Provider } from 'react-redux';
import AppReducer from './reducers';
import PubNub from 'pubnub';
import { PubNubProvider } from "pubnub-react";

//const Drawer = createDrawerNavigator();
const Stack = createStackNavigator();
const store = createStore(AppReducer);
const pubnub = new PubNub({
  publishKey: "pub-c-94adb7c6-a03f-45e7-9b33-1f140f9d53c7",
  subscribeKey: "sub-c-dffd05c8-7b24-11ea-9770-0a12e0cf0d6e",
});
//console.log('test');

function Home() {
  return (
    <Stack.Navigator mode="modal" screenOptions={styles.defaultNavigationOptions}>
      <Stack.Screen name="Splash" component={SplashScreen} options={{ headerShown: false }} />
      {/* <Stack.Screen name="Login" component={LoginScreen} options={{ headerShown: false }} /> */}
      <Stack.Screen name="Welcome" component={WelcomeScreen} options={{ headerShown: false }} />
      <Stack.Screen name="Connected" component={ConnectedScreen} options={{ headerShown: false }} />
      {/* <Stack.Screen name="Register" component={RegisterScreen} options={{ headerShown: false }} /> */}
      <Stack.Screen name="Chat" component={ChatScreen} />
      <Stack.Screen name="Contact" component={ContactScreen} />
      <Stack.Screen name="AddParents" component={AddParentsScreen} />
    </Stack.Navigator>
  );
}

export default class App extends Component {
  state = {
    isLoadingComplete: false,
  };

 

  handleResourcesAsync = async () => {
    return Promise.all([
      Font.loadAsync({
        "Rubik-Regular": require("./assets/fonts/Rubik-Regular.ttf"),
        "Rubik-Black": require("./assets/fonts/Rubik-Black.ttf"),
        "Rubik-Bold": require("./assets/fonts/Rubik-Bold.ttf"),
        "Rubik-Italic": require("./assets/fonts/Rubik-Italic.ttf"),
        "Rubik-Light": require("./assets/fonts/Rubik-Light.ttf"),
        "Rubik-Medium": require("./assets/fonts/Rubik-Medium.ttf"),
        "GoogleSans-Bold": require("./assets/fonts/GoogleSans-Bold.ttf"),
        "GoogleSans-Medium": require("./assets/fonts/GoogleSans-Medium.ttf"),
        "Poppins-Bold": require("./assets/fonts/Poppins-Bold.ttf"),
        'Roboto': require('native-base/Fonts/Roboto.ttf'),
        'Roboto_medium': require('native-base/Fonts/Roboto_medium.ttf'),
        'Pacifico': require('./assets/fonts/Pacifico-Regular.ttf'),

      })
    ]);
  };

  render() {
    if (!this.state.isLoadingComplete && !this.props.skipLoadingScreen) {
      return (
        <AppLoading
          startAsync={this.handleResourcesAsync}
          onError={error => console.warn(error)}
          onFinish={() => this.setState({ isLoadingComplete: true })}
        />
      );
    }
    return (
      <Provider store={store} >
        <NavigationContainer>
          {/* <Drawer.Navigator
            drawerContent={props => <DrawerMenu {...props} />}> */}
          <Home></Home>
          {/* <Drawer.Screen name="DrawerMenu" component={DrawerMenu} /> */}
          {/* </Drawer.Navigator> */}
        </NavigationContainer>
      </Provider>
    )
  }
}

const styles = {
  defaultNavigationOptions: {
    headerStyle: {
      height: 80,
      backgroundColor:"#62cdd9",
      // backgroundColor: "transparent",
      borderBottomColor: "transparent",
      elevation: 0 // for android
    },
    title: null,
    //headerBackImage: () => (<Image source={require("./assets/images/icons/back.png")} style={{ height: 25, width: 25 }} />),
    headerBackTitle: null,
    headerLeftContainerStyle: {
      alignItems: "center",
      marginLeft: 10,
      //paddingRight: theme.sizes.base
    },
    headerRightContainerStyle: {
      alignItems: "center",
      //paddingRight: theme.sizes.base
    },
  }
}