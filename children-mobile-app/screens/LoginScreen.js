import React, { Component } from 'react';
import { View, StyleSheet } from 'react-native';
import { Form, Item, Input, } from 'native-base';
import { Button, Text } from '../components';
import * as SecureStore from 'expo-secure-store';

import { connect } from 'react-redux';
import { Login, Logout } from '../actions';
import { apiUrl } from '../config';
import { TouchableOpacity } from 'react-native-gesture-handler';

const axios = require('axios');

class LoginScreen extends Component {
  state = { username: '', password: '', error: '' }

  onChangeText = (key, val) => {
    this.setState({ [key]: val })
  }

  Login = async (username, password) => {
    const { Login, navigation } = this.props;

    try {
      const res = await axios.post(`${apiUrl}/auth/signin`, {
        username: username,
        password: password
      });
      const usertoken = res.data.token;

      if (!usertoken) {
        this.setState({ error: true });
        return;
      }

      SecureStore.setItemAsync('userToken', usertoken);
      Login(usertoken);

      navigation.navigate('Welcome');
      navigation.reset({
        index: 0,
        routes: [{ name: 'Welcome' }],
      });
    } catch (error) {
      if (error.response.status === 401) {
        this.setState({ error: error.response.data.message });
      }
    }
  }

  async componentDidMount() {
    const { Logout } = this.props;
    await SecureStore.deleteItemAsync('userToken');
    Logout();
  }

  render() {
    const { navigation } = this.props;

    return (
      <View style={styles.container} >
        <View style={styles.top}></View>

        <View style={styles.middle}>
          <Text style={styles.textContainer}>Welcom back!</Text>

          <View style={styles.formArea}>
            <Text style={[styles.textContainer, styles.signin]}>Login</Text>
            <Form style={styles.mainForm}>
              <Item style={styles.formItems}>
                <Input placeholder="Username" style={styles.Input} onChangeText={val => this.onChangeText('username', val)} />
              </Item>

              <Item style={styles.formItems}>
                <Input placeholder="Password" secureTextEntry={true} style={styles.Input} onChangeText={val => this.onChangeText('password', val)} />
              </Item>

              {this.state.error != '' ? (<View>
                <Text center style={styles.error}>{this.state.error}</Text>
              </View>) : null}

              <View style={styles.Button}>
                <Button gradient style={{ width: 160 }} onPress={() => {
                  this.Login(this.state.username, this.state.password);
                }}>
                  <Text bold white center>
                    GO
                  </Text>
                </Button>
              </View>
            </Form>
          </View>
        </View>

        <View style={styles.bottom}>
          <View style={{ bottom: '20%' }} >
            <TouchableOpacity onPress={() => { navigation.navigate('Register') }}>
              <Text style={styles.textBottom}>Don't have an account? Register now</Text>
            </TouchableOpacity>
          </View>
        </View>
      </View>
    )
  }
}

export default connect(state => {
  return {
    data: state.userReducer
  }
}, dispatch => {
  return {
    Login: (usertoken) => dispatch(Login(usertoken)),
    Logout: () => dispatch(Logout())
  }
})(LoginScreen);

const styles = StyleSheet.create({
  container: {
    flex: 1,
    position: 'relative',
  },
  error: {
    color: 'red',
    paddingTop: 20
  },
  top: {
    position: 'relative',
    backgroundColor: '#2196f3',
    paddingRight: 12.7,
    paddingLeft: 12.7,
    height: '30%',
  },
  middle: {
    width: '100%',
    height: '100%',
    flex: 1,
    position: 'absolute',
    zIndex: 2,
    backgroundColor: 'transparent',
    paddingLeft: 26.3,
    paddingRight: 26.3,
  },
  bottom: {
    position: 'relative',
    height: '70%',
    paddingRight: 12.7,
    paddingLeft: 12.7,
    backgroundColor: '#2196f3',
    justifyContent: 'flex-end',
    flexDirection: 'column',
    alignItems: 'center'
  },
  textContainer: {
    color: '#FCFDFF',
    fontFamily: 'GoogleSans-Bold',
    fontSize: 24,
    marginBottom: 30,
    top: '20%',
    position: 'relative',
    alignSelf: 'center'
  },
  formArea: {
    alignSelf: 'center',
    width: '100%',
    backgroundColor: '#ffffff',
    borderRadius: 5,
    top: '20%',
    paddingBottom: 20,
  },
  signin: {
    top: 0,
    color: '#2D3057',
    marginTop: 15,
  },
  formItems: {
    marginTop: 10,
    marginLeft: 20,
    marginRight: 20
  },
  Input: {
    fontFamily: 'GoogleSans-Medium',
    fontSize: 16,
  },
  loginText: {
    color: '#2D3057',
    fontSize: 10,
    fontFamily: 'GoogleSans-Bold',
    fontWeight: 'bold',
  },
  Button: {
    paddingTop: 20,
    borderRadius: 4,
    alignItems: 'center'
  },
  btnText: {
    color: '#FFFFFF',
    fontFamily: 'GoogleSans-Bold',
    fontWeight: 'bold',
    fontSize: 16,
  },
  textBottom: {
    color: '#FCFDFF',
    fontFamily: 'GoogleSans-Bold',
    fontSize: 15,
  }
});
