import React, { Component } from 'react';
import {
  View,
  Image,
  Text,
  StyleSheet,
  Animated,
  ActivityIndicator,
} from 'react-native';

// import Logo from '../assets/logo.png';
import * as SecureStore from 'expo-secure-store';
import { connect } from 'react-redux';
import { RestoreUserToken } from '../actions';

class SplashScreen extends Component {
  state = {
    LogoAnime: new Animated.Value(0),
    LogoText: new Animated.Value(0),
  };

  componentDidMount() {
    const { LogoAnime, LogoText } = this.state;
    const { navigation, RestoreUserToken } = this.props;
    Animated.parallel([
      Animated.spring(LogoAnime, {
        toValue: 1,
        tension: 10,
        friction: 2,
        duration: 1500,
      }).start(),

      Animated.timing(LogoText, {
        toValue: 1,
        duration: 1500,
      }),
    ]).start(async () => {
      // let userToken;

      // userToken = await SecureStore.getItemAsync('userToken');

      // if (userToken) {
      //   RestoreUserToken(userToken);
      //   setTimeout(() => {
      //     navigation.navigate('Welcome');
      //     navigation.reset({
      //       index: 0,
      //       routes: [{ name: 'Welcome' }],
      //     });
      //   }, 1000);
      // }
      // else {
      //   setTimeout(() => { navigation.navigate('Welcome') }, 1000);
      // }
      await SecureStore.deleteItemAsync('UUID');
      const parentKey = await SecureStore.getItemAsync('parentKey');
      const UUID = await SecureStore.getItemAsync('UUID');
      const { navigation } = this.props;

      if (parentKey != null && UUID != null) {
        navigation.navigate('Connected', { parentKey: parentKey, childrenKey: UUID });
      }
      else{
        setTimeout(() => { navigation.navigate('Welcome') }, 1000);
      }
    });
  }

  render() {
    return (
      <View style={styles.container}>
        <Animated.View
          style={{
            opacity: this.state.LogoAnime,
            top: this.state.LogoAnime.interpolate({
              inputRange: [0, 1],
              outputRange: [80, 0],
            }),
          }}>
          {/* <Image source={Logo} /> */}

          {this.state.loadingSpinner ? (
            <ActivityIndicator
              style={{
                position: 'absolute',
                left: 0,
                right: 0,
                top: 0,
                bottom: 0,
                alignItems: 'center',
                justifyContent: 'center',
              }}
              size="large"
              color="#5257f2"
            />
          ) : null}
        </Animated.View>
        <Animated.View style={{ opacity: this.state.LogoText }}>
          <Text style={styles.logoText}> App </Text>
        </Animated.View>
      </View>
    );
  }
}

export default connect(state => {
  return {
    data: state.userReducer
  }
}, dispatch => {
  return {
    RestoreUserToken: (userToken) => dispatch(RestoreUserToken(userToken))
  }
})(SplashScreen);

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#2196f3',
    justifyContent: 'center',
    alignItems: 'center',
  },

  logoText: {
    color: '#FFFFFF',
    fontSize: 30,
    marginTop: 30,
    fontWeight: 'bold',
  },
});
