import React, { Component } from 'react';
import { StyleSheet, View, ToastAndroid, TextInput, Dimensions } from 'react-native';
import PubNubReact from 'pubnub-react';
import * as Location from 'expo-location';
import { Button, Text } from '../components';
import { YellowBox } from "react-native";
import _ from "lodash";
YellowBox.ignoreWarnings(["Setting a timer"]);
const _console = _.clone(console);
console.warn = (message) => {
    if (message.indexOf("Setting a timer") <= -1) {
        _console.warn(message);
    }
};

const screen = Dimensions.get("window");
const WIDTH = screen.width;
const HEIGHT = screen.height;

export default class AddParentsScreen extends Component {
    constructor(props) {
        super(props);

        this.pubnub = new PubNubReact({
            publishKey: "pub-c-94adb7c6-a03f-45e7-9b33-1f140f9d53c7",
            subscribeKey: "sub-c-dffd05c8-7b24-11ea-9770-0a12e0cf0d6e"
        });

        //Base State
        this.state = {
            UUID: '',
            parentKey: '',
            connected: false,
            error: null
        };

        this.pubnub.init(this);
    }

    connect = (childrenKey) => {
        const { navigation } = this.props;

        this.pubnub.subscribe({
            channels: [`${this.state.parentKey}-connect-${this.state.UUID}`],
            withPresence: true
        });

        this.pubnub.publish({
            message: {
                uuid: this.state.UUID,
                key: this.state.parentKey
            },
            channel: `${this.state.parentKey}-connect`
        });


        this.pubnub.getMessage(`${this.state.parentKey}-connect-${this.state.UUID}`, async (msg) => {
            if (msg.message.success) {
                this.setState({ connected: true });
                let position = await Location.getCurrentPositionAsync({
                    enableHighAccuracy: true
                });

                navigation.navigate('Connected', { parentKey: this.state.parentKey, childrenKey });

                setTimeout(() => {
                    this.pubnub.publish({
                        message: {
                            latitude: position.coords.latitude,
                            longitude: position.coords.longitude,
                            uuid: this.state.UUID,
                        },
                        channel: this.state.parentKey
                    }, (status, response) => {
                        if (status.error) {
                            this.setState({ error: "Parent'key is incorect", connected: false })
                        }
                    });
                }, 1000);
            }
        });
    }

    // generateRandomString = (length) => {
    //     let text = "";
    //     const possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";

    //     for (var i = 0; i < length; i++)
    //         text += possible.charAt(Math.floor(Math.random() * possible.length));

    //     return text;
    // }

    async componentDidMount() {
        const { route } = this.props;
        const childrenKey = route.params.childrenKey;
        let { status } = await Location.requestPermissionsAsync();
        this.setState({ UUID: childrenKey });

        if (status !== 'granted') {
            ToastAndroid.show("Permission to access location was denied", ToastAndroid.SHORT);
        }
    }

    async componentDidUpdate() {
        if (!this.state.connected) {
            return;
        }
        const parentKey = this.state.parentKey;
        //Get Stationary Coordinate

        Location.watchPositionAsync(
            {
                enableHighAccuracy: true,
                distanceInterval: 1,
                timeInterval: 10000
            },
            position => {
                this.pubnub.publish({
                    message: {
                        latitude: position.coords.latitude,
                        longitude: position.coords.longitude,
                        uuid: this.state.UUID,
                    },
                    channel: parentKey
                }, (status, response) => {
                    if (status.error) {
                        this.setState({ error: "Parent'key is incorect", connected: false })
                    }
                });
            },
            error => console.log(error)
        );
    }

    onChangeText = (text) => {
        this.setState({ parentKey: text })
    }

    render() {
        const { navigation } = this.props;
        const childrenKey = this.state.UUID;
        return (
            <View style={styles.container}>
                <Text style={{ fontSize: 20 }} color='#554ff7'>ID Trẻ: {this.state.UUID}</Text>

                <Text style={{ fontSize: 22 }}>Nhập khóa của cha mẹ</Text>

                <TextInput
                    style={{ height: 50, width: '60%', borderWidth: 1, marginTop: 30, marginBottom: 10, textAlign: 'center', borderRadius: 15 }}
                    onChangeText={text => this.onChangeText(text.toUpperCase())}
                />
                <Button gradient style={[{ width: WIDTH * 0.4, margin: 10 }]}
                    onPress={(childrenKey) => this.connect(childrenKey)}
                // onPress={() => {
                //     navigation.navigate('Connected', { parentKey: this.state.parentKey, childrenKey });
                // }}
                >
                    <Text center white title bold>Kết nối</Text>
                </Button>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center'
    }
});