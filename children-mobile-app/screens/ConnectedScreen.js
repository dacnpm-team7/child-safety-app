import React, { Component } from 'react';
import {
    StyleSheet,
    View,
    ToastAndroid,
    TextInput,
    Dimensions,
    StatusBar,
    Image,
    ScrollView
} from 'react-native';
import { theme } from "../constants";
import PubNubReact from 'pubnub-react';
import * as Location from 'expo-location';
import { Button, Text } from '../components';
import { YellowBox } from "react-native";
import _ from "lodash";
YellowBox.ignoreWarnings(["Setting a timer"]);
const _console = _.clone(console);
console.warn = (message) => {
    if (message.indexOf("Setting a timer") <= -1) {
        _console.warn(message);
    }
};

const screen = Dimensions.get("window");
const WIDTH = screen.width;
const HEIGHT = screen.height;
const HomeImage = require("../assets/icons/home.png");
const callIcon = require("../assets/icons/call.png");
const chatIcon = require("../assets/icons/chat.png");
const listIcon = require("../assets/icons/list.png");
const addIcon = require("../assets/icons/addUser.png");
const BackIcon = require('../assets/icons/back.png');

export default class ConnectedScreen extends Component {
    constructor(props) {
        super(props);
        this.state = {
            index: 0
        };
    }
    render() {
        const { route, navigation } = this.props;
        const parentKey = route.params.parentKey;
        const childrenKey = route.params.childrenKey;
        return (
            <View style={{
                flex: 1,
                flexDirection: 'column',
                alignItems: 'center',
            }}>
                <React.Fragment>
                    {/* <ScrollView style={styles.welcome} showsVerticalScrollIndicator={false} > */}
                        <View style={styles.container}>
                            <StatusBar barStyle="light-content" />
                            <View style={{ flexDirection: 'row', width: WIDTH, height: HEIGHT * 0.1, alignItems: 'center', backgroundColor:"#62cdd9" }}>
                                <Text white h2 pacifico style={{ height: HEIGHT * 0.1, flex: 1, left:10, top:5}}>Find Kids</Text>
                                <View style={{ flex: 1, justifyContent: 'flex-end', flexDirection: 'row' }}>
                                    <Button color='#ff0000' style={{ flexDirection: 'row', alignItems: 'center', height: HEIGHT * 0.06, width: WIDTH * 0.18, borderRadius: 35, right:10 }}>
                                        <Text white title center bold>SOS</Text>
                                    </Button>
                                </View>
                            </View>
                            <Image source={HomeImage} style={{height:WIDTH*0.7, width:WIDTH*0.7, marginTop:10}}></Image>
                            <Button color='#62cdd9' style={{ width: WIDTH * 0.8, borderRadius: WIDTH * 0.05, marginTop: HEIGHT * 0.03, height: WIDTH * 0.17 }}
                                onPress={() => { navigation.navigate('Contact', { parentKey: parentKey, childrenKey: childrenKey }); }}>
                                <View style={{ fontSize: WIDTH * 0.1, flexDirection: 'row', marginLeft: WIDTH * 0.025, width: WIDTH * 0.7 }}>
                                    <Image source={chatIcon} style={{ resizeMode: "cover", width: WIDTH * 0.07, height: WIDTH * 0.07, marginRight: WIDTH * 0.02 }} />
                                    <Text bold white left style={{ marginTop: HEIGHT * 0.005, width: WIDTH * 0.6, fontSize: WIDTH * 0.05 }}>Liên hệ</Text>
                                    <Image source={BackIcon} style={{ transform: [{ rotate: "180deg" }], width: WIDTH * 0.05, height: WIDTH * 0.05, marginTop: WIDTH * 0.01 }} />
                                </View>
                            </Button>
                            <Button color='#62cdd9' style={{ width: WIDTH * 0.8, borderRadius: WIDTH * 0.05, marginTop: HEIGHT * 0.03, height: WIDTH * 0.17 }}
                                onPress={() => { navigation.navigate('AddParents', { parentKey: parentKey, childrenKey: childrenKey }); }}>
                                <View style={{ fontSize: WIDTH * 0.1, flexDirection: 'row', marginLeft: WIDTH * 0.025, width: WIDTH * 0.7 }}>
                                    <Image source={addIcon} style={{ resizeMode: "cover", width: WIDTH * 0.07, height: WIDTH * 0.07, marginRight: WIDTH * 0.02 }} />
                                    <Text bold white left style={{ marginTop: HEIGHT * 0.005, width: WIDTH * 0.6, fontSize: WIDTH * 0.05 }}>Thêm phụ huynh</Text>
                                    <Image source={BackIcon} style={{ transform: [{ rotate: "180deg" }], width: WIDTH * 0.05, height: WIDTH * 0.05, marginTop: WIDTH * 0.01 }} />
                                </View>
                            </Button>
                            <Button color='#62cdd9' style={{ width: WIDTH * 0.8, borderRadius: WIDTH * 0.05, marginTop: HEIGHT * 0.03, height: WIDTH * 0.17 }}>
                                <View style={{ fontSize: WIDTH * 0.1, flexDirection: 'row', marginLeft: WIDTH * 0.025, width: WIDTH * 0.7 }}>
                                    <Image source={listIcon} style={{ resizeMode: "cover", width: WIDTH * 0.07, height: WIDTH * 0.07, marginRight: WIDTH * 0.02 }} />
                                    <Text bold white left style={{ marginTop: HEIGHT * 0.005, width: WIDTH * 0.6, fontSize: WIDTH * 0.05 }}>Thời gian biểu</Text>
                                    <Image source={BackIcon} style={{ transform: [{ rotate: "180deg" }], width: WIDTH * 0.05, height: WIDTH * 0.05, marginTop: WIDTH * 0.01 }} />
                                </View>
                            </Button>

                        </View>
                    {/* </ScrollView> */}
                </React.Fragment>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
        height: HEIGHT,
        // backgroundColor:'blue'
        // justifyContent: 'center'
    }
});