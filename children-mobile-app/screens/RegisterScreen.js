import React, { Component } from 'react';
import { View, StyleSheet } from 'react-native';
import { Form, Item, Input, } from 'native-base';
import { Button, Text } from '../components';
import * as SecureStore from 'expo-secure-store';

import { connect } from 'react-redux';
import { Login } from '../actions';
import { apiUrl } from '../config';

const axios = require('axios');

class RegisterScreen extends Component {
  state = { username: '', password: '', confirmPassword: '', error: '', success: false }

  onChangeText = (key, val) => {
    var st = this.state;
    st = { ...st, [key]: val };
    st.password == st.confirmPassword ? this.setState({ [key]: val, error: '' }) : this.setState({ [key]: val, error: 'Confirm password is not match' });
  }

  doLogin = (username, password) => {
    const { Login, navigation } = this.props;

    axios.post(`${apiUrl}/auth/signin`, {
      username: username,
      password: password
    })
      .then((response) => {
        const userToken = response.data.token;
        if (!userToken) {
          this.setState({ error: "Can't login, try again later" });
          return;
        }
        SecureStore.setItemAsync('userToken', userToken);
        Login(userToken);

        navigation.navigate('Welcome');
        navigation.reset({
          index: 0,
          routes: [{ name: 'Welcome' }],
        });
      })
      .catch((error) => {
        if (error.response.status === 401) {
          this.setState({ error: error.response.data.message });
        }
      });
  }

  Register = async (username, password) => {
    if (this.state.error != '') {
      return;
    }

    try {
      const res = await axios.post(`${apiUrl}/auth/signup`, {
        username: username,
        password: password
      });

      const user = res.data;

      if (!user) {
        this.setState({ error: 'Create your account failed' });
        return;
      }

      this.doLogin(username, password);
    } catch (error) {
      if (error.response.status === 400) {
        this.setState({ error: error.response.data.message });
      }
    }
  }

  render() {
    return (
      <View style={styles.container} >
        <View style={styles.top}></View>

        <View style={styles.middle}>
          <Text style={styles.textContainer}>You are ready to go</Text>

          <View style={styles.formArea}>
            <Text style={[styles.textContainer, styles.signin]}>Registration</Text>
            <Form style={styles.mainForm}>
              <Item style={styles.formItems}>
                <Input placeholder="Username" style={styles.Input} onChangeText={val => this.onChangeText('username', val)} />
              </Item>

              <Item style={styles.formItems}>
                <Input placeholder="Password" secureTextEntry={true} style={styles.Input} onChangeText={val => this.onChangeText('password', val)} />
              </Item>

              <Item style={styles.formItems}>
                <Input placeholder="Confirm password" secureTextEntry={true} style={styles.Input} onChangeText={val => this.onChangeText('confirmPassword', val)} />
              </Item>

              {this.state.error != '' ? (<View>
                <Text center style={styles.error}>{this.state.error}</Text>
              </View>) : null}

              <View style={styles.Button}>
                <Button gradient style={{ width: 160 }} onPress={() => {
                  this.Register(this.state.username, this.state.password);
                }}>
                  <Text bold white center>
                    GO
                  </Text>
                </Button>
              </View>
            </Form>
          </View>
        </View>

        <View style={styles.bottom}>
        </View>
      </View>
    )
  }
}

export default connect(state => {
  return {
    data: state.userReducer
  }
}, dispatch => {
  return {
    Login: (userToken) => dispatch(Login(userToken))
  }
})(RegisterScreen);

const styles = StyleSheet.create({
  container: {
    flex: 1,
    position: 'relative',
  },
  error: {
    color: 'red',
    paddingTop: 20,
    paddingLeft: 20
  },
  top: {
    position: 'relative',
    backgroundColor: '#2196f3',
    paddingRight: 12.7,
    paddingLeft: 12.7,
    height: '30%',
  },
  middle: {
    width: '100%',
    height: '100%',
    flex: 1,
    position: 'absolute',
    zIndex: 2,
    backgroundColor: 'transparent',
    paddingLeft: 26.3,
    paddingRight: 26.3,
  },
  bottom: {
    position: 'relative',
    height: '70%',
    paddingRight: 12.7,
    paddingLeft: 12.7,
    backgroundColor: '#2196f3',
    justifyContent: 'flex-end',
    flexDirection: 'column',
    alignItems: 'center'
  },
  textContainer: {
    color: '#FCFDFF',
    fontFamily: 'GoogleSans-Bold',
    fontSize: 24,
    marginBottom: 30,
    top: '15%',
    position: 'relative',
    alignSelf: 'center'
  },
  formArea: {
    alignSelf: 'center',
    width: '100%',
    backgroundColor: '#ffffff',
    borderRadius: 5,
    top: '15%',
    paddingBottom: 20,
  },
  signin: {
    top: 0,
    color: '#2D3057',
    marginTop: 15,
  },
  formItems: {
    marginTop: 10,
    marginLeft: 20,
    marginRight: 20
  },
  Input: {
    fontFamily: 'GoogleSans-Medium',
    fontSize: 16,
  },
  loginText: {
    color: '#2D3057',
    fontSize: 10,
    fontFamily: 'GoogleSans-Bold',
    fontWeight: 'bold',
  },
  Button: {
    paddingTop: 20,
    borderRadius: 4,
    alignItems: 'center'
  },
  btnText: {
    color: '#FFFFFF',
    fontFamily: 'GoogleSans-Bold',
    fontWeight: 'bold',
    fontSize: 16,
  },
  textBottom: {
    color: '#FCFDFF',
    fontFamily: 'GoogleSans-Bold',
    fontSize: 15,
  }
});