import React, { Component } from 'react';
import {
    StyleSheet,
    View,
    ToastAndroid,
    TextInput,
    Dimensions,
    StatusBar,
    Image,
    ScrollView
} from 'react-native';
import { theme } from "../constants";
import PubNubReact from 'pubnub-react';
import * as Location from 'expo-location';
import { Button, Text } from '../components';
import { YellowBox } from "react-native";
import _ from "lodash";
YellowBox.ignoreWarnings(["Setting a timer"]);
const _console = _.clone(console);

console.warn = (message) => {
    if (message.indexOf("Setting a timer") <= -1) {
        _console.warn(message);
    }
};

const screen = Dimensions.get("window");
const WIDTH = screen.width;
const HEIGHT = screen.height;
const ParentsIcon = require("../assets/icons/parents.png");
const homeIcon = require("../assets/icons/home.png");
const callIcon = require("../assets/icons/call.png");
const chatIcon = require("../assets/icons/chat.png");
const listIcon = require("../assets/icons/list.png");
const addIcon = require("../assets/icons/addUser.png");
const BackIcon = require('../assets/icons/back.png');

export default class ContactScreen extends Component {
    constructor(props) {
        super(props);
        this.state = {
            index: 0
        };
    }

    componentDidMount() {
        const { navigation } = this.props;
        navigation.setOptions({
            headerTitle: "Liên hệ",
            headerTitleStyle: {
                textAlign: 'center',
                fontWeight: 'normal',
                fontSize: 20,
                color: 'black',
            },
            headerTitleAlign: 'center'
        });
    }

    render() {
        const { route, navigation } = this.props;
        const parentKey = route.params.parentKey;
        const childrenKey = route.params.childrenKey;
        return (
            <View style={{
                flex: 1,
                flexDirection: 'column',
                alignItems: 'center',
            }}>
                <React.Fragment>
                    <ScrollView style={styles.welcome} showsVerticalScrollIndicator={false} >
                        <View style={styles.container}>
                            <StatusBar barStyle="light-content" />
                            <Button color='#ed5a5a' style={{ width: WIDTH * 0.8, borderRadius: WIDTH * 0.05, marginTop: HEIGHT * 0.15, height: HEIGHT * 0.12 }}
                                onPress={() => {console.log('children key:',childrenKey); navigation.navigate('Chat', { parentKey: parentKey, childrenKey: childrenKey }); }}>
                                <View style={{ fontSize: WIDTH * 0.1, flexDirection: 'row', marginLeft: WIDTH * 0.025, width: WIDTH * 0.7 }}>
                                    <Image source={ParentsIcon} style={{ resizeMode: "cover", width: WIDTH * 0.07, height: WIDTH * 0.07, marginRight: WIDTH * 0.02 }} />
                                    <Text white left style={{ marginTop: HEIGHT * 0.005, width: WIDTH * 0.6, fontSize: WIDTH * 0.05 }}>Ba</Text>
                                    <Image source={BackIcon} style={{ transform: [{ rotate: "180deg" }], width: WIDTH * 0.05, height: WIDTH * 0.05, marginTop: WIDTH * 0.01 }} />
                                </View>
                            </Button>
                            <Button color='#278c7a' style={{ width: WIDTH * 0.8, borderRadius: WIDTH * 0.05, marginTop: HEIGHT * 0.05, height: HEIGHT * 0.12 }}
                                onPress={() => {navigation.navigate('Chat', { parentKey: parentKey, childrenKey: childrenKey }); }}>
                                <View style={{ fontSize: WIDTH * 0.1, flexDirection: 'row', marginLeft: WIDTH * 0.025, width: WIDTH * 0.7 }}>
                                    <Image source={ParentsIcon} style={{ resizeMode: "cover", width: WIDTH * 0.07, height: WIDTH * 0.07, marginRight: WIDTH * 0.02 }} />
                                    <Text white left style={{ marginTop: HEIGHT * 0.005, width: WIDTH * 0.6, fontSize: WIDTH * 0.05 }}>Mẹ</Text>
                                    <Image source={BackIcon} style={{ transform: [{ rotate: "180deg" }], width: WIDTH * 0.05, height: WIDTH * 0.05, marginTop: WIDTH * 0.01 }} />
                                </View>
                            </Button>
                        </View>
                    </ScrollView>
                </React.Fragment>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
        height: HEIGHT,
    }
});